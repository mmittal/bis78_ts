/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#ifndef BIS78_triggerSimulation_h
#define BIS78_triggerSimulation_h
#include <vector>

#include "AthenaBaseComps/AthAlgorithm.h"

#include "GaudiKernel/Property.h"
#include "GaudiKernel/ServiceHandle.h"
#include "GaudiKernel/INTupleSvc.h"
#include "GaudiKernel/NTuple.h"

#include "GaudiKernel/ServiceHandle.h"

#include "StoreGate/DataHandle.h"
#include "Identifier/Identifier.h"

#include "RPCcablingInterface/IRPCcablingServerSvc.h"

#include "TrigT1RPClogic/RPCsimuData.h"
#include "TrigT1RPClogic/CMAdata.h"
#include "TrigT1RPClogic/PADdata.h"
#include "TrigT1RPClogic/SLdata.h"
#include "TrigT1RPClogic/RPCbytestream.h"
#include  "MuonRDO/RpcBis78_TrigRawDataContainer.h"
#include <vector>
//#include "TrigT1RPCmonitoring/TrigEfficiency.h"

#include "MuonReadoutGeometry/MuonDetectorManager.h"

#define DEFAULT_L1MuctpiStoreLocationRPC "/Event/L1MuctpiStoreRPC"

class RpcIdHelper;




/////////////////////////////////////////////////////////////////////////////

class BIS78_triggerSimulation : public AthAlgorithm {
  
public:
  BIS78_triggerSimulation (const std::string& name, ISvcLocator* pSvcLocator);
  ~BIS78_triggerSimulation();
  
  StatusCode  initialize();
  StatusCode  execute();
  StatusCode  find_coincidence(RPCsimuData &data,const std::unique_ptr<Muon::RpcBis78_TrigRawDataContainer>& trgContainer);
  StatusCode AddStrip(int mphi, int igap, int strip);
  unsigned int find_trigword();
  //private:

  const MuonGM::MuonDetectorManager*   mbis_MuonMgr;
  const RpcIdHelper*                   mbis_rpcId;


  std::vector<int> m_strip_eta[3];
  std::vector<int> m_strip_phi[3];

  BIS78_triggerSimulation * pad[2][8];
};


#endif
