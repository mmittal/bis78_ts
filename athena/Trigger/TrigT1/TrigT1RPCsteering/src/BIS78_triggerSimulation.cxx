
/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/



#include "GaudiKernel/SmartDataPtr.h"

#include "GeneratorObjects/McEventCollection.h"  // include for retrieving the Montecarlo
#include "HepMC/GenEvent.h"                      // thru

#include "TrigT1RPClogic/ShowData.h"

#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/ISvcLocator.h"
#include "GaudiKernel/StatusCode.h"

#include "GaudiKernel/PropertyMgr.h"
#include "GaudiKernel/IToolSvc.h"

#include "MuonIdHelpers/RpcIdHelper.h"


#include "MuonDigitContainer/RpcDigitContainer.h"
#include "MuonDigitContainer/RpcDigitCollection.h"
#include "MuonDigitContainer/RpcDigit.h"

#include "MuonReadoutGeometry/RpcReadoutElement.h"

// next candidate for removal
//#include "TrigT1RPCmonitoring/DetailedTW.h" 

#include "TrigT1RPChardware/SectorLogic.h"
#include "TrigT1RPChardware/MatrixReadOut.h"

#include "TrigT1RPClogic/decodeSL.h"

#include "TrigT1Interfaces/Lvl1MuCTPIInput.h"
#include <algorithm>
#include <cmath>

using namespace std;

//static double time_correction(double, double, double);

static int digit_num = 0;
static int digit_out = 0;


#include "TrigT1RPCsteering/BIS78_triggerSimulation.h"
#include <iostream>
#include <algorithm>
#include <cmath>

using namespace std;

//static double time_correction(double, double, double);




// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 

//
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

BIS78_triggerSimulation::BIS78_triggerSimulation(const std::string& name, ISvcLocator* pSvcLocator):
  AthAlgorithm(name, pSvcLocator)
 //BIS78_triggerSimulation::BIS78_triggerSimulation()
{
  mbis_MuonMgr=0;
  mbis_rpcId =0;




}

BIS78_triggerSimulation::~BIS78_triggerSimulation()
{;}



StatusCode BIS78_triggerSimulation::initialize(){
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.

  std::cout<< "Monika, I am in BIS78_triggerSimulation"<< std::endl;

  //   MsgStream mylog(msgSvc(), name());
  // ATH_MSG_INFO("Initializing");
  CHECK(detStore()->retrieve( mbis_MuonMgr ));
  mbis_rpcId = mbis_MuonMgr->rpcIdHelper();

  for (int i=0; i<3; i++){
    m_strip_eta[i].clear();
    m_strip_phi[i].clear();
  }
  return StatusCode::SUCCESS;
}


StatusCode BIS78_triggerSimulation::AddStrip(int mphi, int igap, int strip){
  if (igap<1||igap>3) return false;
  if (mphi==1){ 
    m_strip_phi[igap-1].push_back(strip);
  } else {
    m_strip_eta[igap-1].push_back(strip); 
  }
  //  std::cout<< " in Add strip mphi, igap, strip " << mphi << " , "<< igap <<" ,"<< strip << std::endl;

   return StatusCode::SUCCESS;
}


StatusCode BIS78_triggerSimulation::execute(){
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.

  std::cout<< "Monika, I am in BIS78_triggerSimulation"<< std::endl;

  std::cout<<m_strip_phi[0].size() << " "<<m_strip_phi[1].size() << " "<<m_strip_phi[2].size() << " "<<m_strip_eta[0].size() << " "<<m_strip_eta[1].size() << " "<<m_strip_eta[2].size() << std::endl;
  return StatusCode::SUCCESS;
}


unsigned int BIS78_triggerSimulation::find_trigword()
// take the data from TrigT1RPC
{
  //  auto trgRawData=std::make_unique< Muon::RpcBis78_TrigRawData>((uint16_t)(sector), (uint16_t)(bcid));
  //  const std::unique_ptr<Muon::RpcBis78_TrigRawDataContainer>& trgContainer;
  unsigned int trigword = 0;  
  const int delta_strip=1;
  int majority_eta=0; int position_eta=-999;
  int majority_phi=0; int position_phi=-999;

  std::cout<<"monika I am in BIS78_triggerSimulation in find_trigword()"<< std::endl;
    
  if(m_strip_phi[0].size()>0 && m_strip_phi[1].size()>0  && m_strip_phi[2].size()>0 && m_strip_eta[0].size()>0 && m_strip_eta[1].size()>0  && m_strip_eta[2].size()>0){
    std::cout<<"monika I am in BIS78_triggerSimulation in find_coincidence() and printin out the number of strips in phi and eta:  " <<m_strip_phi[0].size() << " "<<m_strip_phi[1].size() << " "<<m_strip_phi[2].size() << " "<<m_strip_eta[0].size() << " "<<m_strip_eta[1].size() << " "<<m_strip_eta[2].size() << std::endl;
      
    // START SEARCH OF PHI COINCIDENCES
    // LOOK FOR at least 2 strips within +-1 strip in two out of three gaps
    for (int i=0; i<m_strip_phi[1].size(); i++){  // loop over all strip in central gap
      int majority=0;
      for (int j=0; j<m_strip_phi[0].size(); j++){ // look for coincidence with first gap
	if (abs(m_strip_phi[1].at(i)-m_strip_phi[0].at(j))<=delta_strip) {
	  majority=1;
	  if (majority>majority_phi){ // if this is the best coincidence so far update best coincidence
	    position_phi=m_strip_phi[1].at(i);
	    majority_phi=majority;
	  }
	}
      }
      for (int j=0; j<m_strip_phi[2].size(); j++){
	if (abs(m_strip_phi[1].at(i)-m_strip_phi[2].at(j))<=delta_strip) {
	  if (majority>0) {
	    majority=2;
	  } else {
	    majority=1;
	  }
	  if (majority>majority_phi){ // if this is the best coincidence so far update best coincidence
	    position_phi=m_strip_phi[1].at(i);
	    majority_phi=majority;
	  }
	}
      }
    }// end loop on central
    if (majority_phi==0){ // try also combination first-last
      for (int i=0; i<m_strip_phi[0].size(); i++){  
	for (int j=0; j<m_strip_phi[2].size(); j++){  // loop over first and last gap
	  if (abs(m_strip_phi[0].at(i)-m_strip_phi[2].at(j))<=delta_strip) {
	    position_phi=m_strip_phi[0].at(i);
	    majority_phi=1;	  
	  } 
	}
      }
    }
    
    // START SEARCH OF ETA COINCIDENCE
    for (int i=0; i<m_strip_eta[1].size(); i++){  // loop over all strip in central gap
      int majority=0;
      for (int j=0; j<m_strip_eta[0].size(); j++){ // look for coincidence with first gap
	if (abs(m_strip_eta[1].at(i)-m_strip_eta[0].at(j))<=delta_strip) {
	  majority=1;
	  if (majority>majority_eta){ // if this is the best coincidence so far update best coincidence
	    position_eta=m_strip_eta[1].at(i);
	    majority_eta=majority;
	  }
	}
      }
      for (int j=0; j<m_strip_eta[2].size(); j++){
	if (abs(m_strip_eta[1].at(i)-m_strip_eta[2].at(j))<=delta_strip) {
	  if (majority>0) {
	    majority=2;
	  } else {
	    majority=1;
	  }
	  if (majority>majority_eta){ // if this is the best coincidence so far update best coincidence
	    position_eta=m_strip_eta[1].at(i);
	    majority_eta=majority;
	  }
	}
      }
    }// end loop on central
    if (majority_eta==0){ // try also combination first-last
      for (int i=0; i<m_strip_eta[0].size(); i++){  
	for (int j=0; j<m_strip_eta[2].size(); j++){  // loop over first and last gap
	  if (abs(m_strip_eta[0].at(i)-m_strip_eta[2].at(j))<=delta_strip) {
	    position_eta=m_strip_eta[0].at(i);
	    majority_eta=1;	  
	  } 
	}
      }
    }
    
    // Check if coincidence etaphi !!
    
    
    if (majority_eta>0&&majority_phi>0){
      unsigned int flag3o3_eta=0;
      if (majority_eta>1)  flag3o3_eta=1;
      unsigned int flag3o3_phi=0;
      if (majority_phi>1)  flag3o3_phi=1;
      
      // set the trigger word
      trigword = ((position_eta & 0x3f) << 18)
	+ ((position_phi & 0x3f) << 12 )
	+ ((flag3o3_eta & 0x1)  <<  5 )
	+ ((flag3o3_phi & 0x1)  <<  4 );
    }
    
    //auto rdo_segment= std::make_unique<Muon::RpcBis78_TrigRawDataSegment>(position_eta,position_phi,trigword );
    //trgRawData->push_back(std::move(rdo_segment));
    //trgContainer->push_back(std::move(trgRawData));
    std::cout<< "Coincidence found for    : " 
	     << " posision_eta    " << position_eta
	     << " posision_phi    " << position_phi
	     << " majority_eta    " << majority_eta
	     << " majority_phi    " << majority_phi 
	     << " Trigword         " << trigword
	     << std::endl;
    
    //  std::cout<< trigword<< std::endl;  

    
  }
  
  return trigword;
}









