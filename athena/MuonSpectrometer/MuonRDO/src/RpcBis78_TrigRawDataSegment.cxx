/*
  Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration
*/  

#include "MuonRDO/RpcBis78_TrigRawDataSegment.h"

Muon::RpcBis78_TrigRawDataSegment::RpcBis78_TrigRawDataSegment() :
  m_etaIndex(0),
  m_phiIndex(0),
  m_trigword(0)
{ }

Muon::RpcBis78_TrigRawDataSegment::RpcBis78_TrigRawDataSegment(uint8_t etaIndex, uint8_t phiIndex, uint8_t trigword ) :
  m_etaIndex(etaIndex),
  m_phiIndex(phiIndex),
  m_trigword(trigword)
{ }

