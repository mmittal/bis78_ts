/*
  Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration
*/  

#ifndef RpcBis78_TRIGRAWDATA_H
#define RpcBis78_TRIGRAWDATA_H

#include "DataModel/DataVector.h"
#include "SGTools/CLASS_DEF.h"

#include "MuonRDO/RpcBis78_TrigRawDataSegment.h"

namespace Muon {
class RpcBis78_TrigRawData : public DataVector<Muon::RpcBis78_TrigRawDataSegment>
{

 public:
  RpcBis78_TrigRawData();
  RpcBis78_TrigRawData(uint16_t sectorId, uint16_t bcId);
  ~RpcBis78_TrigRawData() { };

  uint16_t sectorId() {return m_sectorId;}
  uint16_t bcId() {return m_bcId;}

 private:

  uint16_t m_sectorId;
  uint16_t m_bcId;

};
}

CLASS_DEF(Muon::RpcBis78_TrigRawData,140250087,1)

#endif   ///  RpcBis78_TRIGRAWDATA_H




