/*
  Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration
*/  

#ifndef RpcBis78_TRIGRAWDATASEGMENT_H
#define RpcBis78_TRIGRAWDATASEGMENT_H

#include "DataModel/DataVector.h"
#include "SGTools/CLASS_DEF.h"


namespace Muon {
class RpcBis78_TrigRawDataSegment
{

 public:
  RpcBis78_TrigRawDataSegment();

  RpcBis78_TrigRawDataSegment(uint8_t etaIndex, uint8_t phiIndex, uint8_t trigword );
  ~RpcBis78_TrigRawDataSegment() { };

  uint8_t etaIndex() {return m_etaIndex;}
  uint8_t phiIndex() {return m_phiIndex;}
  uint8_t trigword() {return m_trigword;}


 private:

  uint8_t m_etaIndex;
  uint8_t m_phiIndex;
  uint8_t m_trigword;

};
}

CLASS_DEF(Muon::RpcBis78_TrigRawDataSegment,218364457,1)

#endif   ///  RpcBis78_TRIGRAWDATASEGMENT_H




