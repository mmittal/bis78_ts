/*
  Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration
*/  

#ifndef RpcBis78_TRIGRAWDATACONTAINER_H
#define RpcBis78_TRIGRAWDATACONTAINER_H

#include "DataModel/DataVector.h"
#include "SGTools/CLASS_DEF.h"

#include "MuonRDO/RpcBis78_TrigRawData.h"

namespace Muon {
  class RpcBis78_TrigRawDataContainer : public DataVector<Muon::RpcBis78_TrigRawData> {

 public:
  RpcBis78_TrigRawDataContainer();
  ~RpcBis78_TrigRawDataContainer() { };

 private:


};
}

CLASS_DEF(Muon::RpcBis78_TrigRawDataContainer,1172141273,1)

#endif   /// RpcBis78_TRIGRAWDATACONTAINER_H





