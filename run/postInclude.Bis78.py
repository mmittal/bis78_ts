
from GeoModelSvc.GeoModelSvcConf import GeoModelSvc
GeoModelSvc = GeoModelSvc()
GeoModelSvc.MuonVersionOverride = "MuonSpectrometer-R.08.01-BIS78v3"

from AthenaCommon.AppMgr import ServiceMgr
import MuonRPC_Cabling.MuonRPC_CablingConfig

#######################
#  CHOICE OF CABLING  #
#######################
## CABLING FROM FILES
ServiceMgr.MuonRPC_CablingSvc.RPCMapfromCool=False
ServiceMgr.MuonRPC_CablingSvc.ConfFileName="LVL1confAtlasRUN2_bis78v1.data" # default is "ConfFileName.txt"
ServiceMgr.MuonRPC_CablingSvc.CorrFileName="MuonRPC_Cabling/LVL1confAtlasRUN2_ver104.corr" # default is "ConfFileName.txt"
######################
# CABLING FROM LOCAL TEST db
#ServiceMgr.MuonRPC_CablingSvc.RPCMapfromCool=True
#from IOVDbSvc.CondDB import conddb
#conddb.addFolder("RPC","/RPC/CABLING/MAP_SCHEMA<tag>RPCCablingMapSchema_MC16</tag><dbConnection>sqlite://;schema=RPC_CABLING_TOT.db;dbname=OFLP200</dbConnection>",force=True)
#conddb.addFolder("RPC","/RPC/CABLING/MAP_SCHEMA_CORR<tag>RPCCablingMapSchemaCorr_MC16</tag><dbConnection>sqlite://;schema=RPC_CABLING_TOT.db;dbname=OFLP200</dbConnection>",force=True)
#conddb.addFolder("RPC","/RPC/CABLING/MAP_SCHEMA<tag>RPCCablingMapSchema_2017_01</tag><dbConnection>sqlite://;schema=Cabling_2016-2017.db;dbname=CONDBR2</dbConnection>",force=True)
#conddb.addFolder("RPC","/RPC/CABLING/MAP_SCHEMA<tag>RPCCablingMapSchemaCorr_2017_01</tag><dbConnection>sqlite://;schema=Cabling_2016-2017.db;dbname=CONDBR2</dbConnection>",force=True)
#conddb.blockFolder("/RPC/CABLING/MAP_SCHEMA")
#conddb.blockFolder("/RPC/CABLING/MAP_SCHEMA_CORR")
###########################
# CHOICE OF TRIGGER ROADS #
###########################
# ROADS ALL OPEN 
ServiceMgr.MuonRPC_CablingSvc.RPCTriggerRoadsfromCool=False
ServiceMgr.MuonRPC_CablingSvc.DatabaseRepository="DUMMY";
#######################
# ROADS FROM ASCII FILES
#ServiceMgr.MuonRPC_CablingSvc.RPCTriggerRoadsfromCool=False
#ServiceMgr.MuonRPC_CablingSvc.DatabaseRepository="MuonRPC_Cabling/RUN3_roads.test";
#######################
# ROADS FROM LOCAL db
#ServiceMgr.MuonRPC_CablingSvc.RPCTriggerRoadsfromCool=True
#from IOVDbSvc.CondDB import conddb
#conddb.addFolder("RPC","/RPC/TRIGGER/CM_THR_ETA<tag>RPCTriggerCMThrEta_RUN12_MC16_01</tag><dbConnection>sqlite://;schema=RPC_TRIGGER_CM_THR_ETA_ALL.db;dbname=OFLP200</dbConnection>",force=True)
#conddb.addFolder("RPC","/RPC/TRIGGER/CM_THR_PHI<tag>RPCTriggerCMThrPhi_RUN12_MC16_01</tag><dbConnection>sqlite://;schema=RPC_TRIGGER_CM_THR_PHI_ALL.db;dbname=OFLP200</dbConnection>",force=True)
#conddb.blockFolder("/RPC/TRIGGER/CM_THR_ETA")
#conddb.blockFolder("/RPC/TRIGGER/CM_THR_PHI")
###conddb.addFolderSplitMC("RPC","/RPC/TRIGGER/CM_THR_ETA", "/RPC/TRIGGER/CM_THR_ETA")
###conddb.addFolderSplitMC("RPC","/RPC/TRIGGER/CM_THR_PHI", "/RPC/TRIGGER/CM_THR_PHI")
#########################



















