#ifndef bis78pad_h
#define bis78pad_h
class bis78pad {
public :
  bis78pad(int iside, int isec);
  ~bis78pad();
  void Clear();
  bool AddStrip(int mphi, int igap, int strip);
  unsigned int execute();

  int m_sector =-1;
  int m_side=-1;
  
 private:
  std::vector<int> m_strip_eta[3];
  std::vector<int> m_strip_phi[3];

  
};
#endif
