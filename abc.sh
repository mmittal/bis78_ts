setupATLAS
lsetup git
git atlas init-workdir -b 21.3 https://:@gitlab.cern.ch:8443/atlas/athena.git
cd athena/
git atlas addpkg IdDictParser
git atlas addpkg MuonReadoutGeometry
git atlas addpkg MuonGeoModel
git atlas addpkg MuonSimEvent
git atlas addpkg MuonG4
git atlas addpkg MuonDigitization
git atlas addpkg MuonRDO
git atlas addpkg TrigT1c
git atlas addpkg MuonCablings

cd ..
mkdir build
mkdir run
op package_filters.txt
asetup 21.3,Athena,r11
cd build
cmake -DATLAS_PACKAGE_FILTER_FILE=../package_filters.txt ../athena/Projects/WorkDir 
make -j10
cd ../
cd run/
source ../build/x86_64-centos7-gcc62-opt/setup.sh 
scp /afs/cern.ch/work/m/mmittal/private/BIS_24Jan/run/*.py .
